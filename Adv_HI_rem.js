/*
	@name Advanced HI text remove
	@version 1.0
	@summary Advanced HI text removal, keeping/making dialogs
    @author 2023 Miklós Balázs <villanyora@yahoo.co.uk>

    SPDX-License-Identifier: GPL-2.0-or-later
*/

var s = subtitle.instance();
for(var i = s.linesCount() - 1; i >= 0; i--) {
	var line = s.line(i),
		text = line.richPrimaryText()
            .replace(/\): /gm, ') ')
            .replace(/\]: /gm, '] ')
			.replace(/-? *\([^)]+\) */gm, '- ')           //replace text in () with possible "- " before it with "- "
			.replace(/-? *\[[^\]]+\] */gm, '- ')          //replace text in [] with possible "- " before it with "- "
    if(text.substring(0,3) == '- <'){                       //put the dialog mark inside the italic and/or bold tag
        text = text.substring(2).replace(/>(?!<)/, '>- ');
    }
    text = text
            .replace(/(<i>♪)(.*\n)*.*(♪<\/i>)/, '')         //remove lyrics
            .replace(/(<i>♪).*(♪\s<\/i>)/, '')              //remove lyrics
            .replace(/\b[A-Ž_\- .'\d]*\d?:(\s|(-\s))/gm, '- ')
            .replace(/-  */, '- ')
            .replace(/- ?$|-- ?$/gm, '')
            .replace(/\n- ?<|-- ?</gm, '<')
            .replace(/- - |-- /gm, '- ').trim();
    if((text.substring(0,2) == '- ' | text.search('>- ') > 0)  && text.match(/- /gm).length == 1)
        text = text.replace('- ', '');
    if(text.match(/\n.*- /gm) != null && text.match(/^(<.*>)*- */) == null)
        text = '- ' + text;
    if(text.match(/- /gm) != null && text.match(/- /gm).length > 1 && text.search('\n- ') == -1)
        text = text.substring(0, text.indexOf('- ', 8)) + "\n" + text.substring(text.indexOf('- ', 8));
    text = text.replace(/(^\s*|\s*$)/gm, '');
    if(text.match(/\n/gm) != null && text.match(/\n/gm).length > 1) {           //unbreak lines with more than 2 rows
        text = text.replace(/\s*\n\s*/gm, ' ');
        if(text.indexOf("- ", 8) != -1){                                        //rebreak if it's a dialog, or
            text = text.substring(0, text.indexOf("- ", 8)-1) + "\n" + text.substring(text.indexOf("- ", 8));
        }else{                                                                  //at closest space after half.
            text = text.substring(0, text.indexOf(" ", text.length/2)) + "\n" + text.substring(text.indexOf(" ", text.length/2)+1);
        }    
    }
	if(text.replace(/(<[^>]+>|\s|-)/gm, '') == '')
		s.removeLine(i);
	else
		line.setRichPrimaryText(text);
}
