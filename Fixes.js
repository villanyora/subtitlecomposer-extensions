/*
	@name Fixes
	@version 1.0
	@summary Fixes some common errors.
	@author 2023 Miklós Balázs <villanyora@yahoo.co.uk>

    SPDX-License-Identifier: GPL-2.0-or-later
*/
const s = subtitle.instance();
for(let i = s.linesCount() - 1; i >= 0; i--) {
	let line = s.line(i),
		text = line.richPrimaryText()
            .replace(/\[\s*/gm, "\[")                       //[ John] -> [John]
            .replace(/\s*\]/gm, "\]")                       //[John ] -> [John]
            .replace(/\(\s*/gm, "\(")                       //( John) -> (John)
            .replace(/\s*\)/gm, "\)")                       //(John ) -> (John)
            .replace(/\bl\s/gm, 'I ')                       //"l think" -> "I think"
            .replace(/\bl\'/gm, 'I\'')                      //"l'm" -> "I'm"
            .replace(/\bl-/gm, 'I...\s')                    //"l-" -> "I... " (stuttering)
            .replace(/\blf /gm, 'If ')                      //"lf you" -> "I you"
            .replace(/\bls /gm, 'Is ')                      //"ls it you" -> "Is it you"
            .replace(/\bl\. /gm, 'I. ')                     //"just you and l." -> "just you and I."
            .replace(/\bl\? /gm, 'I? ')                     //"just you and l? " -> "just you and I? "
            .replace(/\bl! /gm, 'I! ')                      //"just you and l! " -> "just you and I! "
            .replace(/\bl, /gm, 'I, ')                      //"you and l, we are" -> "you and I, we are"
            .replace(/\blt /gm, 'It ')                      //"lt does" -> "It does"
            .replace(/\blt\'/gm, 'It\'')                    //"lt's me" -> "It's me"
            .replace(/\bln /gm, 'In ')                      //"ln there" -> "In there"
            .replace(/\s+:/gm, ':')                         //remove space(s) before ":"
            .replace(/-(?! )/gm, "- ")                      //add missing space after "-"
            .replace(/\. \. \./, '...')                     //"I. . . don't know" -> "I... don't know"
            .replace(/--/gm, '...')
            .replace(/—/gm, '...');
        while(text.search(/[A-Z]l[A-Z]/) > 0){              //replace lower case l with uppercase I between uppercase letters
	        text = text.substring(0, text.search(/[A-Z]l[A-Z]/)+1) + "I" + text.substring(text.search(/[A-Z]l[A-Z]/)+2);
        }
        while(text.search(/\d(\s+|,)\d/gm) >= 0)            //remove space and comma between numbers "1 200 miles" or "1,200 miles" -> "1200 miles"
            text = text.substring(0, text.search(/\d(\s|,)\d/gm)+1) + text.substring(text.search(/\d(\s|,)\d/gm)+2);
        while(text.search(/\d:\s+\d/gm) >= 0)               //fix time or score "15: 30" -> "15:30"
            text = text.substring(0, text.search(/\d:\s+\d/gm)+2) + text.substring(text.search(/\d:\s+\d/gm)+3).trim();
        while(text.search(/,\w|\!\w|\?\w|\.\w/g) > 0){         //add missing space after punctuation mark (e.g comma, e.mark, q.mark and period)
            text = text.substring(0, text.search(/,\w|\!\w|\?\w|\.\w/g)+1) + " " + text.substring(text.search(/,\w|!\w|\?\w|\.\w/g)+1);
        }
        while(text.search(/\d\.\s\d/g) >= 0){                  //remove (just added) space after dot between numbers "Scores 2. 5." -> "Scores 2.5."
            text = text.substring(0, text.search(/\d\.\s\d/g)+2) + text.substring(text.search(/\d\.\s\d/g)+3);
        }
        while(text.search(/[A-Ž]- [A-Ž]/gm) >= 0){             //remove (just added) space after "-" between letters "Peek- a- boo" -> "Peek-a-boo"
            text = text.substring(0, text.search(/[A-Ž]- [A-Ž]/gm)+2) + text.substring(text.search(/[A-Ž]- [A-Ž]/gm)+3);
        }
        while(text.search(/[A-Ž]\.\s+[A-Ž]\./gm) >= 0){        //remove (just added) space in abbreviations ("A. M." -> "A.M." or "p. m." -> "p.m.")
	        text = text.substring(0, text.search(/[A-Ž]\.\s+[A-Ž]\./gm)+2) + text.substring(text.search(/[A-Ž]\.\s+[A-Ž]\./gm)+2).trim();
        }
        text = text.replace(/\.\.\. +/gm, '...');            //remove (just added) space after "..."
        if(text.match(/\n/gm) != null && text.match(/\n/gm).length > 1) {           //unbreak lines with more than 2 rows
            text = text.replace(/\s*\n\s*/gm, ' ');
            if(text.indexOf("- ", 8) != -1){                                        //rebreak if it's a dialog, or
                text = text.substring(0, text.indexOf("- ", 8)-1) + "\n" + text.substring(text.indexOf("- ", 8));
            }else{                                                                  //at closest space after half.
                text = text.substring(0, text.indexOf(" ", text.length/2)) + "\n" + text.substring(text.indexOf(" ", text.length/2)+1);
            }    
        }
		line.setRichPrimaryText(text);
}
