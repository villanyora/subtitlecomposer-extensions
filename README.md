# Subtitle Composer Extensions
A few extensions for Subtitle Composer based on the examples provided there in the Tools menu.

Read about the extension and if you like it or just want to test it, download the .js file and add it to Subtitle Composer under Tools/Script Manager.

**IMPORTANT:** To make these extensions work, have the latest git version of Subtitle Composer installed on your system, instead of the one to be found in the default repository, which may not have the lates bugfixes!
For instructions see [this page.](https://subtitlecomposer.kde.org/download.html)
Choose your system and click on the repository button to find the instructions.

**HINT:** The added extensions can be mapped to any (free) key-combination under Settings->Configure Keyboard Shortcuts (Ctrl+Alt+.), so one does not have to browse for them every time in the Tools menu, when needed.

- Settings -> Configure Keyboard Shortcuts
- In the popup window the extension you added (e.g. Lyrics.js)
- Choose custom and click on "None"
- Enter the key-combination you want. If it's not taken SC will accept it, otherwise you get a warning about conflict.
- When done, click on Manage Schemes and under More Actions save the new shortcut


# **1. Adv_HI_rem.js** -> (I mapped this to Alt+H)

There's a default extension in SC to remove Hearing Impaired text which works great, however I personally needed one that did a little ... "more", so I enhanced it to fit my own needs.

Let me be clear: there are so many different versions/ways of marking HI text in a subtitle that there's no 100% way of always removing all of them AND keeping the remaining lines intact. I tried to cover as much versions as possible, but you may have a line that I missed. If so, please let me know!

My goal was to remove all HI text AND to mark/keep the dialogs in tact, or marking them as such if they weren't.
Just an example:
```
JOHN: Are you ready?
JANE: Absolutely.
```
would turn into:
```
- Are you ready?
- Absolutely.
```

# **2. Lyrics.js** -> (I mapped this to Alt+L)

This is a simple one. Based on the SC-delivered example "Iterate Selected Lines" I extended/changed it to mark or unmark the selected lines as lyrics of songs, turning lines like:

We will, we will rock you!

into:

_♪ We will, we will rock you! ♪_

and vica versa.

# **3. Dialog.js** -> (I mapped this to Alt+D)

Again a simple one. Just like Lyrics.js this one iterates through the selected lines as well, but this one marks or unmarks the row(s) in them as dialogs.

From:

```
Are you ready?
Absolutely.
```
to

```
- Are you ready?
- Absolutely.
```
and vica versa.

# **4. Fixes.js** -> (I mapped this to Alt+U)

This one contains a collection of fixes to common (OCR) errors. It may not suit your needs, so I commented each step to give you and idea of what to remove or how to rewrite it, if needed.

Enjoy!
